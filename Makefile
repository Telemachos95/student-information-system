stucs: chained_hashing.o main.o operations.o skiplist.o 
	gcc -o stucs chained_hashing.o main.o operations.o skiplist.o -g

chained_hashing.o: chained_hashing.c skiplist.h chained_hashing.h
	gcc -c chained_hashing.c -o chained_hashing.o

main.o: main.c chained_hashing.h operations.h
	gcc -c main.c -o main.o

operations.o: operations.c skiplist.h chained_hashing.h operations.h
	gcc -c operations.c -o operations.o

skiplist.o: skiplist.c skiplist.h
	gcc -c skiplist.c -o skiplist.o

clean :
	rm chained_hashing.o main.o operations.o skiplist.o
