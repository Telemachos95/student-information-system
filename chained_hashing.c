#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "skiplist.h"
#include "chained_hashing.h"

int elementCount = 10;

struct hashNode * createHashNode(record *ptr) {
    struct hashNode *newnode;
    newnode = (struct hashNode *) malloc(sizeof (struct hashNode));
    newnode->ptr = ptr;
    newnode->next = NULL;
    return newnode;
}

void insertToHash(record *ptr, struct hash *hashTable) {
    int hashIndex = ptr->postcode % elementCount;
    struct hashNode *newnode = createHashNode(ptr);
    /* head of list for the bucket with index "hashIndex" */
    if (!hashTable[hashIndex].head) {
        hashTable[hashIndex].head = newnode;
        hashTable[hashIndex].count = 1;
        return;
    }
    /* adding new node to the list */
    newnode->next = (hashTable[hashIndex].head);
    /*
     * update the head of the list and no of
     * nodes in the current bucket
     */
    hashTable[hashIndex].head = newnode;
    hashTable[hashIndex].count++;
    return;
}

void deleteFromHash(record *ptr, struct hash *hashTable) {
    /* find the bucket using hash index */
    int hashIndex = ptr->postcode % elementCount, flag = 0;
    struct hashNode *temp, *myNode;
    /* get the list head from current bucket */
    myNode = hashTable[hashIndex].head;
    if (!myNode) {
        printf("No element with this postcode in the hash Table\n");
        return;
    }
    temp = myNode;
    while (myNode != NULL) {
        /* delete the node with given key */
        if (myNode->ptr->postcode == ptr->postcode) {
            flag = 1;
            if (myNode == hashTable[hashIndex].head) {
                hashTable[hashIndex].head = myNode->next;
            } else {
                temp->next = myNode->next;
                hashTable[hashIndex].count--;
                free(myNode);
                break;
            }
        }
        temp = myNode;
        myNode = myNode->next;
    }
    if (flag) {
        printf("Element deleted successfully from Hash Table %d\n", ptr->studid);
    } else {
        printf("No element with this postcode in the hash Table\n");
    }
    return;
}

void searchInHash(record *ptr, struct hash *hashTable) {
    int hashIndex = ptr->postcode % elementCount, flag = 0;
    struct hashNode *myNode;
    myNode = hashTable[hashIndex].head;
    if (!myNode) {
        printf("Search element unavailable in hash table\n");
        return;
    }
    while (myNode != NULL) {
        if (myNode->ptr->postcode == ptr->postcode) {
            printf("studid  : %d\n", myNode->ptr->studid);
            printf("lastname     : %s\n", myNode->ptr->lastname);
            printf("deprt      : %s\n", myNode->ptr->deprt);
            flag = 1;
            break;
        }
        myNode = myNode->next;
    }
    if (!flag)
        printf("Search element unavailable in hash table\n");
    return;
}

void display(struct hash *hashTable) {
    struct hashNode *myNode;
    int i;
    for (i = 0; i < elementCount; i++) {
        if (hashTable[i].count == 0)
            continue;
        myNode = hashTable[i].head;
        if (!myNode)
            continue;
        printf("\nData at index %d in Hash Table:\n", i);
        printf("studid     lastname          postcode   \n");
        printf("--------------------------------\n");
        while (myNode != NULL) {
            printf("%-12d", myNode->ptr->studid);
            printf("%-15s", myNode->ptr->lastname);
            printf("%d\n", myNode->ptr->postcode);
            myNode = myNode->next;
        }
    }
    return;
}

float averageHash(struct hash *hashTable, int postcode) {
    float gpasum = 0;
    int count = 0;
    struct hashNode *myNode;
    int i;
    for (i = 0; i < elementCount; i++) {
        if (hashTable[i].count == 0) {
            continue;
        }
        myNode = hashTable[i].head;
        if (!myNode) {
            continue;
        }
        while (myNode != NULL) {
            if (myNode->ptr->postcode == postcode) {
                gpasum += myNode->ptr->gpa;
                count++;
            }
            myNode = myNode->next;
        }
    }
    float result = gpasum / count;
    return result;
}

char *topHash(struct hash *hashTable, int k, int postcode) {
    struct hashNode *myNode;
    record **top = malloc(sizeof (record *) * k);
    int i;
    int count = 0;
    float tmp = 0;

    for (i = 0; i < elementCount; i++) {
        if (hashTable[i].count == 0) {
            continue;
        }
        myNode = hashTable[i].head;
        if (!myNode) {
            continue;
        }
        while (myNode != NULL) {
            if (myNode->ptr->postcode == postcode) {
                if (count < k) {
                    top[count] = myNode->ptr;
                    count++;
                }
                if (count == k) {
                int x;
                int j;
                    for ( x = 0; x < k; x++) {
                        for ( j = 0; j < k - x; j++) {
                            if (top[j]->gpa > top[j + 1]->gpa) {
                                tmp = top[j]->gpa;
                                top[j]->gpa = top[j + 1]->gpa;
                                top[j + 1]->gpa = tmp;
                            }
                        }
                    }
                    int temp;
                    for ( temp = 0; temp < k; temp++) {
                        if (top[temp]->gpa <= myNode->ptr->gpa) {
                            top[temp]->gpa = myNode->ptr->gpa;
                            break;
                        }
                    }
                }
            }
            myNode = myNode->next;
        }
    }
    int var;
    for (var = 0; var < k; var++) {
        char *result;
        sprintf(result, "%f", top[var]->gpa);
        return result;
    }
}

char **courses_Hash(struct hash *hashTable, int postcode, char *department) {
    struct hashNode *myNode;
    int i;
    for (i = 0; i < elementCount; i++) {
        if (hashTable[i].count == 0) {
            continue;
        }
        myNode = hashTable[i].head;
        if (!myNode) {
            continue;
        }
        while (myNode != NULL) {
            if (myNode->ptr->postcode == postcode && myNode->ptr->deprt == department) {
                char **result = malloc(3 * sizeof (char*));
                sprintf(result[0], "%d", myNode->ptr->postcode);
                result[1] = myNode->ptr->deprt;
                sprintf(result[2], "%d", myNode->ptr->numofcourses);
                return result;
            }
            myNode = myNode->next;
        }
    }
}

float percentHash(struct hash *hashTable, int postcode) {
    int postcodesum = 0;
    int count = 0;
    struct hashNode *myNode;
    int i;
    for (i = 0; i < elementCount; i++) {
        if (hashTable[i].count == 0) {
            continue;
        }
        myNode = hashTable[i].head;
        if (!myNode) {
            continue;
        }
        while (myNode != NULL) {
            count++;
            if (myNode->ptr->postcode == postcode) {
                postcodesum++;
            }
            myNode = myNode->next;
        }
	
    }
	
    float result =  (float)postcodesum / count * 100;
    return result;
}

char **percentagesHash(struct hash *hashTable) {
    int postcodesum = 0;
    struct hashNode *myNode;
    int i;

    for (i = 0; i < elementCount; i++) {
        int *postcodes = malloc(sizeof (int)*hashTable[i].count);
        int *frequencies = malloc(sizeof (int)*hashTable[i].count);
        if (hashTable[i].count == 0) {
            continue;
        }
        myNode = hashTable[i].head;
        if (!myNode) {
            continue;
        }
        int x;
        for ( x = 0; x < hashTable[i].count; x++) {
            postcodes[x] = myNode->ptr->postcode;
            myNode = myNode->next;
        }

        int count, j,var;
        for (var = 0; var < hashTable[i].count; var++) {
            count = 1;
            for (j = var + 1; j < hashTable[i].count; j++) {
                if (postcodes[var] == postcodes[j]) {
                    count++;
                    frequencies[j] = 0;
                }
            }

            if (frequencies[var] != 0) {
                frequencies[var] = count;
            }
        }
        int temp;
        for (temp = 0; temp < hashTable[i].count; temp++) {
            if (frequencies[temp] != 0) {
                char **result = malloc(2 * sizeof (char*));
                sprintf(result[0], "%d", postcodes[temp]);
                float percentage = frequencies[temp] / hashTable->records * 100;
                sprintf(result[1], "%f", percentage);
                return result;
            }
        }

    }

}
