#ifndef CHAINED_HASHING_H
#define	CHAINED_HASHING_H
#endif	


struct hashNode {
    record *ptr;
    struct hashNode *next;
};

struct hash {
    struct hashNode *head;
    int count;
    int records;
};

struct node * createNode(record *ptr);
void insertToHash(record *ptr, struct hash *hashTable);
void deleteFromHash(record *ptr, struct hash *hashTable);
void searchInHash(record *ptr, struct hash *hashTable);
void display();
float averageHash(struct hash *hashTable, int postcode);
char *topHash(struct hash *hashTable,int k,int postcode);
char **courses_Hash(struct hash *hashTable,int postcode,char *department);
float percentHash(struct hash *hashTable, int postcode);
char **percentagesHash(struct hash *hashTable);
