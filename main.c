#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "skiplist.h"
#include "chained_hashing.h"
#include "operations.h"

int main(int argc, char *argv[]) {

    int executions = argv[1];

    for (int i = 0; i < executions; i++) {

        skiplist *list;
        list = (skiplist *) malloc(sizeof (skiplist));
        skiplist_init(list);

        struct hash *hashTable;
        hashTable = (struct hash *) calloc(10, sizeof (struct hash));

        FILE * fp;
        char *filename;
        filename = argv[1];
        char * line = NULL;
        size_t len = 0;
        ssize_t read;

        fp = fopen(filename, "r");
        if (fp == NULL) {
            printf("error");
        }

        while ((read = getline(&line, &len, fp)) != -1) {

            char *check = strtok(line, " ");

            if (strcmp(check, "i") == 0) {
                insert(line, list, hashTable);
                hashTable->records++;
            } else if (strcmp(check, "q") == 0) {
                // query(line,list);
            } else if (strcmp(check, "m") == 0) {
                //            modify(line,list,hashTable);
            } else if (strcmp(check, "d") == 0) {
                //                deleteNode(line, list, hashTable);
            } else if (strcmp(check, "ra") == 0) {
                //                        raverage(line,list);
            } else if (strcmp(check, "a") == 0) {
                average(line, hashTable);
            } else if (strcmp(check, "ta") == 0) {
                //                        taverage(line,hashTable);
            } else if (strcmp(check, "b") == 0) {
                //                        bottom(line,list);
            } else if (strcmp(check, "ct") == 0) {
                courses_to_take(line, hashTable);
            } else if (strcmp(check, "f") == 0) {
                //                        find(line,list);
            } else if (strcmp(check, "p") == 0) {
                percentile(line, hashTable);
            } else if (strcmp(check, "pe") == 0) {
                percentiles(line, hashTable);
            } else if (strcmp(check, "e") == 0 || strcmp(check, " ") == 0) {
                return 0;
            }

        }

        //skiplist_dump(list);
        display(hashTable);

        fclose(fp);

        if (line) {
            free(line);
        }
    }

    return 0;
}
