#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "skiplist.h"
#include "chained_hashing.h"
#include "operations.h"

char *insert(char *data, skiplist *list, struct hash *hashTable) {
    char **array = malloc(10 * sizeof (char*));
    int i = 0;

    while (data != NULL) {
        array[i] = data;
        data = strtok(NULL, " ");
        i++;
        if (i == 8) {
            break;
        }
    }

    record *entry = (record *) malloc(10 * sizeof (record));
    int studid = atoi(array[1]);
    entry->studid = studid;
    strcpy(entry->lastname, array[2]);
    strcpy(entry->firstname, array[3]);
    float ftemp = atof(array[4]);
    entry->gpa = ftemp;
    int numofcourses = atoi(array[5]);
    entry->numofcourses = numofcourses;
    strcpy(entry->deprt, array[6]);
    int postcode = atoi(array[7]);
    entry->postcode = postcode;

    skiplist_insert(list, studid, entry);
    insertToHash(entry, hashTable);

    return "";
}

char *query(char *data, skiplist *list) {
    char **array = malloc(10 * sizeof (char*));
    int i = 0;

    while (data != NULL) {
        array[i] = data;
        data = strtok(NULL, " ");
        i++;
        if (i == 2) {
            break;
        }
    }

    int studid = atoi(array[1]);

    node *result = skiplist_search(list, studid);
    if (result != NULL) {
        //        printf("ok \n");
    }

    return "";
}

char *modify(char *data, skiplist *list, struct hash *hashTable) {
    char **array = malloc(10 * sizeof (char*));
    int i = 0;

    while (data != NULL) {
        array[i] = data;
        data = strtok(NULL, " ");
        i++;
        if (i == 4) {
            break;
        }
    }

    int studid = atoi(array[1]);
    record *rec = (record *) malloc(10 * sizeof (record));
    node *entry = skiplist_search(list, studid);
    rec->postcode = entry->ptr->postcode;

    if (entry != NULL) {
        skiplist_delete(list, studid);
        deleteFromHash(rec, hashTable);
        rec->studid = studid;
        strcpy(rec->lastname, entry->ptr->lastname);
        strcpy(rec->firstname, entry->ptr->firstname);
        float ftemp = atof(array[2]);
        rec->gpa = ftemp;
        int numofcourses = atoi(array[3]);
        rec->numofcourses = numofcourses;
        strcpy(rec->deprt, entry->ptr->deprt);
        rec->postcode = entry->ptr->postcode;

        skiplist_insert(list, studid, rec);
        insertToHash(rec, hashTable);
    } else {
        printf("error");
    }
    return "";
}

char *deleteNode(char *data, skiplist *list, struct hash * hashTable) {

    char **array = malloc(2 * sizeof (char*));
    int i = 0;

    while (data != NULL) {
        array[i] = data;
        data = strtok(NULL, " ");
        i++;
        if (i == 2) {
            break;
        }
    }

    int studid = atoi(array[1]);
    record *rec = (record *) malloc(sizeof (record));
    node *entry = skiplist_search(list, studid);
    rec->postcode = entry->ptr->postcode;
    skiplist_delete(list, studid);
    deleteFromHash(rec, hashTable);

    return "";
}

char *raverage(char *data, skiplist *list) {
    char **array = malloc(10 * sizeof (char*));
    int i = 0;

    while (data != NULL) {
        array[i] = data;
        data = strtok(NULL, " ");
        i++;
        if (i == 3) {
            break;
        }
    }

    int studid1 = atoi(array[1]);
    int studid2 = atoi(array[2]);
    float rav = skiplist_raverage(list, studid1, studid2);
    printf(" %f\n", rav);

    return "";
}

char *average(char *data, struct hash * hashTable) {
    char **array = malloc(10 * sizeof (char*));
    int i = 0;

    while (data != NULL) {
        array[i] = data;
        data = strtok(NULL, " ");
        i++;
        if (i == 2) {
            break;
        }
    }

    int postcode = atoi(array[1]);
    float average = averageHash(hashTable, postcode);
    printf("%.2f\n", average);

    return "";
}

char *taverage(char *data, struct hash * hashTable) {
    char **array = malloc(10 * sizeof (char*));
    int i = 0;

    while (data != NULL) {
        array[i] = data;
        data = strtok(NULL, " ");
        i++;
        if (i == 3) {
            break;
        }
    }

    int k = atoi(array[1]);
    int postcode = atoi(array[2]);
    char *result = topHash(hashTable, k, postcode);
    printf(" %s\n",result);
    return "";
}

char *bottom(char *data, skiplist *list) {
    char **array = malloc(10 * sizeof (char*));
    int i = 0;

    while (data != NULL) {
        array[i] = data;
        data = strtok(NULL, " ");
        i++;
        if (i == 2) {
            break;
        }
    }

    int k = atoi(array[1]);
    char *result = skiplistBottom(list, k);
    return "";
}

char *courses_to_take(char *data, struct hash * hashTable) {
    char **array = malloc(10 * sizeof (char*));
    int i = 0;

    while (data != NULL) {
        array[i] = data;
        data = strtok(NULL, " ");
        i++;
        if (i == 3) {
            break;
        }
    }

    int postcode = atoi(array[1]);
    char *department = malloc(10 * sizeof (char));
    strcpy(department, array[2]);
    char **result = courses_Hash(hashTable, postcode, department);
    return "";
}

char *find(char *data, skiplist *list) {
    char **array = malloc(10 * sizeof (char*));
    int i = 0;

    while (data != NULL) {
        array[i] = data;
        data = strtok(NULL, " ");
        i++;
        if (i == 2) {
            break;
        }
    }

    float gpa = atof(array[1]);
    char **result = skiplistFind(list, gpa);
    return "";
}

char *percentile(char *data, struct hash * hashTable) {
    char **array = malloc(10 * sizeof (char*));
    int i = 0;

    while (data != NULL) {
        array[i] = data;
        data = strtok(NULL, " ");
        i++;
        if (i == 2) {
            break;
        }
    }

    int postcode = atoi(array[1]);
    float result = percentHash(hashTable, postcode);
    char *res = malloc(100 * sizeof(char));
    sprintf(res, "%.2f", result);
    printf("%s\n",res);
    return res;
}

char *percentiles(char *data, struct hash * hashTable) {
    char **result = percentagesHash(hashTable);
    return "";
}



