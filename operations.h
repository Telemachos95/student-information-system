#ifndef OPERATIONS_H
#define	OPERATIONS_H
#endif	/* OPERATIONS_H */

char *insert(char *data,skiplist *list,struct hash *hashTable);
char *query(char *data, skiplist *list);
char *modify(char *data, skiplist *list, struct hash *hashTable);
char *deleteNode(char *data, skiplist *list, struct hash * hashTable);
char *raverage(char *data, skiplist *list);
char *average(char *data, struct hash * hashTable);
char *taverage(char *data, struct hash * hashTable);
char *bottom(char *data, skiplist *list);
char *courses_to_take(char *data, struct hash * hashTable);
char *find(char *data, skiplist *list);
char *percentile(char *data, struct hash * hashTable);
char *percentiles(char *data, struct hash * hashTable);
