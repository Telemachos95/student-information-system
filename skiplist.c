#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <time.h>
#include "skiplist.h"

skiplist *skiplist_init(skiplist *list) {
    int i;
    node *header = (node *) malloc(sizeof (node));
    list->header = header;
    header->key = INT_MAX;

    for (i = 0; i <= MaxLevel - 1; i++) {
        header->forward[i] = (node *) malloc(
                sizeof (node *));
    }

    for (i = 0; i <= MaxLevel - 1; i++) {
        header->forward[i] = list->header;
    }

    list->level = 1;

    return list;
}

int randomLevel() {
    int level = 0;
    srand(time(NULL));
    level = rand() % MaxLevel - 1;

    return level;
}

node *makeNode(int level, int key, record *ptr) {
    int i;
    node *x = (node *) malloc(sizeof (node));
    x->key = key;
    x->ptr = ptr;
    for (i = 0; i <= level; i++) {
        x->forward[i] = (node *) malloc(
                sizeof (node *));
    }
    return x;
}

int skiplist_insert(skiplist *list, int key, record *ptr) {
    node * update[MaxLevel - 1];
    node *x = list->header;
    int i, level;
    for (i = MaxLevel - 1; i >= 0; i--) {
        while (x->forward[i]->key < key) {
            x = x->forward[i];
        }
        update[i] = x;
    }
    x = x->forward[0];

    if (x->key == key) {
        printf("This student id is already submitted.");
        return 0;
    } else {
        level = randomLevel();
        x = makeNode(level, key, ptr);

        for (i = 0; i <= level; i++) {

            x->forward[i] = update[i]->forward[i];
            update[i]->forward[i] = x;
        }
    }
    return 0;
}

node *skiplist_search(skiplist *list, int key) {
    node *x = list->header;
    int i;
    for (i = MaxLevel - 1; i >= 0; i--) {
        while (x->forward[i]->key < key) {
            x = x->forward[i];
        }
    }
    x = x->forward[0];
    if (x->key == key) {
        return x;
    } else {
        //        printf("No record found with this student id \n");
        return NULL;
    }
    return NULL;
}

void skiplist_node_free(node *x) {
    if (x) {
        free(x->forward);
        free(x);
    }
}

int skiplist_delete(skiplist *list, int key) {
    int i;
    node * update[MaxLevel - 1];
    node *x = list->header;
    for (i = MaxLevel - 1; i >= 0; i--) {
        while (x->forward[i]->key < key) {
            x = x->forward[i];
        }
        update[i] = x;
    }
    x = x->forward[0];
    if (x->key == key) {
        for (i = 0; i <= MaxLevel - 1; i++) {
            if (update[i]->forward[i] != x) {
                break;
            }
            update[i]->forward[i] = x->forward[i];
        }
        //skiplist_node_free(x);
    }
    return 1;
}

void skiplist_free(skiplist *list) {
    node *current_node = list->header->forward[0];
    while (current_node != list->header) {
        node *next_node = current_node->forward[0];
        free(current_node->forward);
        free(current_node);
        current_node = next_node;
    }
    free(current_node->forward);
    free(current_node);
    free(list);
}

void skiplist_dump(skiplist *list) {
    node *x = list->header;
    while (x && x->forward[0] != list->header) {
        printf("%d,%d  ", x->ptr->studid, x->ptr->numofcourses);
        x = x->forward[0];
    }
    printf("NIL\n");
}

float skiplist_raverage(skiplist *list, int studid1, int studid2) {
    float gpasum = 0;
    int count = 0;
    node *x = list->header;
    while (x && x->forward[0] != list->header) {
        if (x->ptr->studid >= studid1 && x->ptr->studid <= studid2) {
            gpasum += x->ptr->gpa;
            count++;
        }
        x = x->forward[0];
    }

    float result = gpasum / count;
    return result;
}

char *skiplistBottom(skiplist *list, int k) {
    node *x = list->header;
    record **bottom = malloc(sizeof (record *) * k);
    int count = 0;
    float tmp = 0;
    while (x && x->forward[0] != list->header) {
        if (count < k) {
            bottom[count] = x->ptr;
            count++;
        }
        if (count == k) {
        int y,j;
            for ( y = 0; y < k; y++) {
                for ( j = 0; j < k - y; j++) {
                    if (bottom[j]->gpa > bottom[j + 1]->gpa) {
                        tmp = bottom[j]->gpa;
                        bottom[j]->gpa = bottom[j + 1]->gpa;
                        bottom[j + 1]->gpa = tmp;
                    }
                }
            }
            int w;
            for ( w = k; w >= 0; w--) {
                if (bottom[w]->gpa >= x->ptr->gpa) {
                    bottom[w]->gpa = x->ptr->gpa;
                    break;
                }
            }
        }
        x = x->forward[0];
    }
    int i;
    for ( i = 0; i < k; i++) {
        char *result;
        sprintf(result, "%f", bottom[i]->gpa);
        return result;
    }
}

char **skiplistFind(skiplist *list, float gpa) {
    node *x = list->header;
    int maxcourses = 0;
    while (x && x->forward[0] != list->header) {
        if (x->ptr->numofcourses >= maxcourses) {
            maxcourses = x->ptr->numofcourses;
        }
        x = x->forward[0];
    }
    node *y = list->header;
    while (y && y->forward[0] != list->header) {
        if (y->ptr->gpa >= gpa) {
            char **result = malloc(7 * sizeof (char*));
            sprintf(result[0], "%d", y->ptr->studid);
            result[1] = y->ptr->lastname;
            result[2] = y->ptr->firstname;
            sprintf(result[3], "%f", y->ptr->gpa);
            sprintf(result[4], "%d", y->ptr->numofcourses);
            result[5] = y->ptr->deprt;
            sprintf(result[6], "%d", y->ptr->postcode);
            return result;
        }
        y = y->forward[0];
    }
}

