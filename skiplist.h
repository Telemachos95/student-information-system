#ifndef SKIPLIST_H
#define	SKIPLIST_H
#endif	/* SKIPLIST_H */
#define MaxLevel 15

typedef struct skiplist {
    int level;
    struct node *header;
} skiplist;

typedef struct record {
    int studid;
    char lastname[20];
    char firstname[20];
    float gpa;
    int numofcourses;
    char deprt[20];
    int postcode;
} record;

typedef struct node {
    int key;
    record *ptr;
    struct node *forward[MaxLevel];
} node;


skiplist *skiplist_init(skiplist *list);
int skiplist_insert(skiplist *list, int key, record *ptr);
node *skiplist_search(skiplist *list, int key);
void skiplist_node_free(node *x);
int skiplist_delete(skiplist *list, int key);
void skiplist_free(skiplist *list);
void skiplist_dump(skiplist *list);
float skiplist_raverage(skiplist *list, int studid1, int studid2);
char *skiplistBottom(skiplist *list, int k);
char **skiplistFind(skiplist *list, float gpa);

